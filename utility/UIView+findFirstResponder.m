//
//  UIView+findFirstResponder.m
//  gywb
//
//  Created by zhang qi on 12-6-25.
//  Copyright (c) 2012年 Tiwer Studio. All rights reserved.
//

#import "UIView+findFirstResponder.h"

@implementation UIView (findFirstResponder)
-(UIView *)rootView{
    if (self.superview==nil) {
        return self.superview.rootView;
    }else{
        return self;
    }
}

- (UIView*)findAndResignFirstResponder
{
    if (self.isFirstResponder) {
        [self resignFirstResponder];
        return self;
    }
    for (UIView *subView in self.subviews) {
        if ([subView findAndResignFirstResponder])
            return subView;
    }
    
    return nil;
}

-(void)removeAllSubviews{
    while (self.subviews.count>0) {
        [[self.subviews objectAtIndex:0] removeFromSuperview];
    }
}
@end
