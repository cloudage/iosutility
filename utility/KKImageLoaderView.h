//
//  BBImageLoaderView.h
//  show
//
//  Created by zhang qi on 12-4-19.
//  Copyright (c) 2012年 Tiwer Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol KKImageLoaderViewDelegate
-(void)imageLoaderTouched:(id)sender;
@end

@interface KKImageLoaderView : UIView
@property (nonatomic,assign) id<KKImageLoaderViewDelegate> delegate;
@property (nonatomic,strong) UIImage* image;
@property (nonatomic,strong) UIImage* placeHolderImage;
@property (nonatomic,readonly) NSURL * loadingUrl;
-(void)loadImageFromURL:(NSURL*)url;

+(void)removeCacheForURL:(NSURL*)url;
@end
