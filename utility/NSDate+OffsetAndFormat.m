//
//  NSDate+OffsetAndFormat.m
//  cinema
//
//  Created by zhang qi on 12-5-15.
//  Copyright (c) 2012年 Tiwer Studio. All rights reserved.
//

#import "NSDate+OffsetAndFormat.h"

@implementation NSDate(OffsetAndFormat)

-(BOOL)isToday{
    NSDate * now = [NSDate date];
    return
    self.components.year==now.components.year &&
    self.components.month==now.components.month &&
    self.components.day==now.components.day;
}

-(NSDate *)dateByOffsetMinutes:(int)offsetMinutes{
    return [self dateByAddingTimeInterval:offsetMinutes*60];
}

-(NSDate *)dateByOffsetDays:(int)offsetDays{
    NSDate* date = [self copy];
    
    if (offsetDays!=0) {
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *todayComponents = [gregorian components:(NSDayCalendarUnit |
                                                                   NSMonthCalendarUnit |
                                                                   NSYearCalendarUnit)
                                                         fromDate:date];
        NSInteger theDay = [todayComponents day];
        NSInteger theMonth = [todayComponents month];
        NSInteger theYear = [todayComponents year];
        
        // now build a NSDate object for yourDate using these components
        NSDateComponents *components = [[NSDateComponents alloc] init];
        [components setDay:theDay];
        [components setMonth:theMonth];
        [components setYear:theYear];
        NSDate *thisDate = [gregorian dateFromComponents:components];
        
        // now build a NSDate object for the next day
        NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
        [offsetComponents setDay:offsetDays];
        date = [gregorian dateByAddingComponents:offsetComponents toDate:thisDate options:0];
    }
    
    return date;
}

-(NSString *)stringWithFormat:(NSString *)format{
    NSDateFormatter* fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = format;
    fmt.timeZone = [NSTimeZone defaultTimeZone];
    
    return [fmt stringFromDate:self];
}

-(NSDateComponents*)components{
    return [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:self];
}

-(NSString *)dayOfWeekInChinese{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *weekdayComponents =[gregorian components:NSWeekdayCalendarUnit fromDate:self];
    
    NSInteger weekday = [weekdayComponents weekday];
    // weekday 1 = Sunday for Gregorian calendar
    
    static NSArray* dowArr = nil;
    if (dowArr==nil) {
        dowArr = [NSArray arrayWithObjects:@"星期六",@"星期日",@"星期一",@"星期二",@"星期三",@"星期四",@"星期五",@"星期六",nil];
    }
    
    return [dowArr objectAtIndex:weekday];
}
@end

@implementation NSString(DateWithFormat)
-(NSDate *)dateWithFormat:(NSString *)format andTimeZone:(NSTimeZone *)timezone{
    NSDateFormatter* fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = format;
    fmt.timeZone = timezone;
    
    return [fmt dateFromString:self];
}
-(NSDate *)dateWithFormat:(NSString *)format{
    return [self dateWithFormat:format andTimeZone:[NSTimeZone defaultTimeZone]];
}
@end