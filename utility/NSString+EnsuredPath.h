//
//  NSString+EnsuredPath.h
//  dygz
//
//  Created by 张 琪 on 12-10-31.
//  Copyright (c) 2012年 多彩贵州印象. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (EnsuredPath)
-(NSString *)stringByAppendingPathComponent:(NSString *)str ensurePath:(Boolean)ensure;
@end
