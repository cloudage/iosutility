//
//  UIView+LinearLayout.m
//  yd365
//
//  Created by 张琪 on 13-4-22.
//  Copyright (c) 2013年 多彩贵州印象. All rights reserved.
//

#import "UIView+LinearLayout.h"

@implementation UIView (LinearLayout)
-(void)layoutLinearlyWithPadding:(CGSize)padding
                          andGap:(CGFloat)gap
                 shouldFitHeight:(BOOL)fitHeight{
    CGFloat offset = padding.height;
    CGRect bounds = self.bounds;
    
    for (UIView * v in self.subviews) {
        v.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin;
        
        if (v.hidden) continue;
        
        CGRect f = v.bounds;
        f.origin.x = padding.width;
        f.origin.y = offset;
        f.size.width = bounds.size.width - padding.width*2;

        if (fitHeight) {
            CGSize sz = f.size;
            sz.height = CGFLOAT_MAX;
            sz = [v sizeThatFits:sz];

            f.size.height = sz.height;
        }

        v.frame = f;
        offset+=f.size.height + gap;
    }
    
    CGRect f = self.frame;
    f.size.height = offset - gap + padding.height*2;
    self.frame = f;
}

-(void)layoutLinearlyWithPadding:(CGSize)padding shouldFitHeight:(BOOL)fitHeight{
    [self layoutLinearlyWithPadding:padding
                             andGap:0
                    shouldFitHeight:fitHeight];
}

-(void)layoutLinearlyWithPadding:(CGSize)padding{
    [self layoutLinearlyWithPadding:padding shouldFitHeight:NO];
}

-(void)layoutLinearlyWithPadding:(CGSize)padding
                          andGap:(CGFloat)gap{
    [self layoutLinearlyWithPadding:padding andGap:gap shouldFitHeight:NO];
}

-(void)layoutLinearly{
    [self layoutLinearlyWithPadding:CGSizeZero];
}
@end
