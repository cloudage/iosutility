//
//  NotifyView.h
//  dygz
//
//  Created by 张 琪 on 12-9-27.
//  Copyright (c) 2012年 多彩贵州印象. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotifyView : UIView
@property (nonatomic,strong) NSString* text;
@property (nonatomic,readonly) Boolean isShowing;
@property (nonatomic,assign) CGFloat progress;
@property (nonatomic,assign) BOOL showProgress;

-(void)showInView:(UIView*)view;
-(void)show;

-(void)dismiss;
-(void)dismissAfterDelay:(NSTimeInterval)delay;

+(void)showNotify:(NSString*)text inView:(UIView*)view;
@end
