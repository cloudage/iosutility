//
//  UIView+BorderColor.h
//  yd365
//
//  Created by 张 琪 on 13-4-12.
//  Copyright (c) 2013年 多彩贵州印象. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (BorderColor)
@property (nonatomic,strong) UIColor * borderColor;
@property (nonatomic,assign) CGFloat borderWidth;
@property (nonatomic,assign) CGFloat cornerRadius;
@end
