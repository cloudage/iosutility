//
//  NSDictionary+TrimNSNull.h
//  dygz
//
//  Created by 张 琪 on 12-10-11.
//  Copyright (c) 2012年 多彩贵州印象. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (TrimNSNull)
-(NSDictionary*)dictionaryByRemovingNSNull;
@end

@interface NSMutableDictionary (TrimNSNull)
-(void)removeNSNull;
@end

@interface NSArray (TrimNSNull)
-(NSArray*) arrayByRemovingNSNull;
@end

@interface NSMutableArray (TrimNSNull)
-(void)removeNSNull;
@end