//
//  UIColor+RGBA.m
//  dygz
//
//  Created by 张 琪 on 12-9-19.
//  Copyright (c) 2012年 多彩贵州印象. All rights reserved.
//

#import "UIColor+RGBA.h"

@implementation UIColor (RGBA)
+(UIColor *)colorWithRGBA:(uint32_t)rgba{
    float r = rgba>>24;
    float g = (rgba>>16) & 0xff;
    float b = (rgba>>8) & 0xff;
    float a = rgba & 0xff;
    float f = 0xff;

    return [UIColor colorWithRed:r/f green:g/f blue:b/f alpha:a/f];
}

+(UIColor *)colorWithIntRed:(NSUInteger)red green:(NSUInteger)green blue:(NSUInteger)blue alpha:(NSUInteger)alpha{
    float r = MIN(red,255);
    float g = MIN(green,255);
    float b = MIN(blue,255);
    float a = MIN(alpha,255);
    float f = 0xFF;
    return [UIColor colorWithRed:r/f green:g/f blue:b/f alpha:a/f];
}
@end
