//
//  UIImageView+LoadURLAsync.h
//  dygz
//
//  Created by 张 琪 on 12-9-21.
//  Copyright (c) 2012年 多彩贵州印象. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (LoadURLAsync)
-(void)loadURLAsync:(NSURL*)url withDefaultImage:(UIImage*)defaultImage;
@end
