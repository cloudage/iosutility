//
//  UIImage+ScaleToSize.h
//  gywb
//
//  Created by zhang qi on 12-6-23.
//  Copyright (c) 2012年 Tiwer Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ScaleToSize)
-(UIImage*)imageScaledToSize:(CGSize)size;
@end
