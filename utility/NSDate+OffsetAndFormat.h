//
//  NSDate+OffsetAndFormat.h
//  cinema
//
//  Created by zhang qi on 12-5-15.
//  Copyright (c) 2012年 Tiwer Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate(OffsetAndFormat)
-(NSDate*)dateByOffsetDays:(int)offsetDays;
-(NSDate*)dateByOffsetMinutes:(int)offsetMinutes;
-(NSString*)stringWithFormat:(NSString*)format;
@property (readonly) NSString* dayOfWeekInChinese;
@property (readonly) NSDateComponents* components;
@property (readonly) BOOL isToday;

@end

@interface NSString(DateWithFormat)
-(NSDate*)dateWithFormat:(NSString*)format;
-(NSDate*)dateWithFormat:(NSString *)format andTimeZone:(NSTimeZone*)timezone;
@end
