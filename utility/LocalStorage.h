//
//  LocalStorage.h
//  yd365
//
//  Created by 张琪 on 13-4-27.
//  Copyright (c) 2013年 多彩贵州印象. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalStorage : NSObject
+(NSArray*)arrayOfCategories;
+(void)clearAllCategories;

+(void)addItem:(NSDictionary*)item inCategory:(NSString*)cat withName:(NSString*)name;
+(void)removeItemWithName:(NSString*)name inCategory:(NSString*)cat;
+(void)clearCategory:(NSString*)cat;

+(NSArray*)arrayOfItemsInCategory:(NSString*)cat;
+(NSArray*)arrayOfItemNamesInCategory:(NSString*)cat;
+(NSDictionary*)itemWithName:(NSString*)name inCategory:(NSString*)cat;
+(BOOL)itemExsistsWithName:(NSString*)name inCategory:(NSString*)cat;

+(id)valueOfKey:(id)key inItem:(NSString*)name inCategory:(NSString*)cat;
+(void)setValue:(id)val forKey:(id)key inItem:(NSString*)name inCategory:(NSString*)cat;
@end
