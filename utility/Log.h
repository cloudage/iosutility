//
//  Log.h
//  common
//
//  Created by zhang qi on 12-4-11.
//  Copyright (c) 2012年 Tiwer Studio. All rights reserved.
//


#ifndef common_Log_h
#define common_Log_h

#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] \n" fmt "\n\n\n"), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define DLog(...)
#endif
#define ALog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#ifdef DEBUG
#   define ULog(fmt, ...)  { UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%s\n [Line %d] ", __PRETTY_FUNCTION__, __LINE__] message:[NSString stringWithFormat:fmt, ##__VA_ARGS__]  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]; [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO]; }
#else
#   define ULog(...)
#endif

#endif
