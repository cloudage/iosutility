//
//  UIView+Snapshot.m
//  yd365
//
//  Created by 张琪 on 13-4-25.
//  Copyright (c) 2013年 多彩贵州印象. All rights reserved.
//

#import "UIView+Snapshot.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIView (Snapshot)
-(UIImage *)imageOfSnapshot{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, [UIScreen mainScreen].scale);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}
@end
