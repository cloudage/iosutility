//
//  LocalStorage.m
//  yd365
//
//  Created by 张琪 on 13-4-27.
//  Copyright (c) 2013年 多彩贵州印象. All rights reserved.
//

#import "LocalStorage.h"
#import "Log.h"

@implementation LocalStorage

+(NSString*)root{
    NSArray * pathes = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * docDir = pathes[0];
    return [docDir stringByAppendingPathComponent:@"LocalStorage"];
}

+(NSArray *)arrayOfCategories{
    NSString * root = [self root];
    
    NSMutableArray * cats = [NSMutableArray array];
    
    BOOL isDir = NO;
    
    NSFileManager * fm = [NSFileManager defaultManager];
    
    if ([fm fileExistsAtPath:root isDirectory:&isDir]&&isDir) {
        NSArray * files = [fm subpathsAtPath:root];
        for (NSString * f in files) {
            if ([f isEqual:@"."]||[f isEqual:@".."]) continue;
            
            [cats addObject:f];
        }
    }
    
    return cats;
}

+(void)clearAllCategories{
    NSError * err = nil;
    [[NSFileManager defaultManager] removeItemAtPath:[self root] error:&err];
    if (err!=nil) {
        DLog(@"%@",err.localizedDescription);
    }
}

+(void)addItem:(NSDictionary *)item inCategory:(NSString *)cat withName:(NSString *)name{
    NSString * dir = [[self root] stringByAppendingPathComponent:cat];
    
    NSFileManager * fm = [NSFileManager defaultManager];
    if(![fm fileExistsAtPath:dir]){
        NSError * err = nil;
        [fm createDirectoryAtPath:dir
      withIntermediateDirectories:YES
                       attributes:nil
                            error:&err];
        if (err!=nil) {
            DLog(@"%@",err.localizedDescription);
        }
    }
    
    NSString * path = [dir stringByAppendingPathComponent:name];
    
    NSData * data = [NSKeyedArchiver archivedDataWithRootObject:item];
    if(![data writeToFile:path atomically:YES]){
        DLog(@"can not save %@",path);
    }
}

+(void)removeItemWithName:(NSString *)name inCategory:(NSString *)cat{
    NSString * path = [[[self root] stringByAppendingPathComponent:cat] stringByAppendingPathComponent:name];
    
    NSError * err = nil;
    [[NSFileManager defaultManager] removeItemAtPath:path error:&err];
    if (err!=nil) {
        DLog(@"%@",err);
    }
}

+(void)clearCategory:(NSString *)cat{
    NSString * path = [[self root] stringByAppendingPathComponent:cat];
    NSError * err = nil;
    [[NSFileManager defaultManager] removeItemAtPath:path error:&err];
    if (err!=nil) {
        DLog(@"%@",err);
    }
}

+(NSArray *)arrayOfItemsInCategory:(NSString *)cat{
    NSString * path = [[self root] stringByAppendingPathComponent:cat];
    NSMutableArray * items = [NSMutableArray array];
    
    BOOL isDir = NO;
    
    NSFileManager * fm = [NSFileManager defaultManager];
    
    if ([fm fileExistsAtPath:path isDirectory:&isDir]&&isDir) {
        NSArray * files = [fm subpathsAtPath:path];
        for (NSString * f in files) {
            if ([f isEqual:@"."]||[f isEqual:@".."]) continue;
            
            NSString * itempath = [path stringByAppendingPathComponent:f];
            
            [items addObject:[NSKeyedUnarchiver unarchiveObjectWithFile:itempath]];
        }
    }
    
    return items;
}

+(NSArray *)arrayOfItemNamesInCategory:(NSString *)cat{
    NSString * path = [[self root] stringByAppendingPathComponent:cat];
    NSMutableArray * items = [NSMutableArray array];
    
    BOOL isDir = NO;
    
    NSFileManager * fm = [NSFileManager defaultManager];
    
    if ([fm fileExistsAtPath:path isDirectory:&isDir]&&isDir) {
        NSArray * files = [fm subpathsAtPath:path];
        for (NSString * f in files) {
            if ([f isEqual:@"."]||[f isEqual:@".."]) continue;
            
            [items addObject:f];
        }
    }
    
    return items;
}

+(NSDictionary *)itemWithName:(NSString *)name inCategory:(NSString *)cat{
    NSString * path = [[self root] stringByAppendingPathComponent:cat];
    path = [path stringByAppendingPathComponent:name];
    
    NSFileManager * fm = [NSFileManager defaultManager];
    
    if ([fm fileExistsAtPath:path]) {
        return [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    }else{
        return nil;
    }
}

+(BOOL)itemExsistsWithName:(NSString *)name inCategory:(NSString *)cat{
    NSString * path = [[self root] stringByAppendingPathComponent:cat];
    path = [path stringByAppendingPathComponent:name];
    
    NSFileManager * fm = [NSFileManager defaultManager];
    
    return [fm fileExistsAtPath:path];
}

+(id)valueOfKey:(id)key
         inItem:(NSString *)name
     inCategory:(NSString *)cat{
    NSDictionary * dic = [self itemWithName:name inCategory:cat];
    return dic==nil ? nil : dic[key];
}

+(void)setValue:(id)val
         forKey:(id)key
         inItem:(NSString *)name
     inCategory:(NSString *)cat{
    
    NSMutableDictionary * dic = [self itemWithName:name inCategory:cat].mutableCopy;
    
    if (dic!=nil) {
        if (val!=nil) {
            dic[key] = val;
        }else{
            [dic removeObjectForKey:key];
        }
        
        [self addItem:dic inCategory:cat withName:name];
    }else if (val!=nil) {
        dic = [NSMutableDictionary dictionary];
        dic[key] = val;
        [self addItem:dic inCategory:cat withName:name];
    }
}
@end
