//
//  Utility.h
//  dygz
//
//  Created by 张 琪 on 12-9-10.
//  Copyright (c) 2012年 多彩贵州印象. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EGORefreshTableHeaderView.h"
#import "HttpClient.h"
#import "UIImageView+LoadURLAsync.h"
#import "Log.h"
#import "NSData+LocalCache.h"
#import "NSDate+OffsetAndFormat.h"
#import "NSString+MD5Addition.h"
#import "NSString+removeHtmlMarks.h"
#import "NYOBetterZoomUIScrollView.h"
#import "Reachability.h"
#import "UIDevice+IdentifierAddition.h"
#import "UIImage+ScaleToSize.h"
#import "UIImage+ColorTransform.h"
#import "UIView+findFirstResponder.h"
#import "UIView+SetHiddenAnimated.h"
#import "UIView+BlockInput.h"
#import "UIView+Shadow.h"
#import "UIView+BorderColor.h"
#import "UIView+LinearLayout.h"
#import "UIView+Snapshot.h"
#import "TintPageControl.h"
#import "UIColor+RGBA.h"
#import "NSDictionary+TrimNSNull.h"
#import "NSDictionary+optValue.h"
#import "UIAlertView+Blocks.h"
#import "NSString+EnsuredPath.h"
#import "UIDevice+Hardware.h"
#import "NSString+Check.h"
#import "NotifyView.h"
#import "SolidColorProgressView.h"
#import "LocalStorage.h"

#if __IPHONE_OS_VERSION_MAX_ALLOWED < 60000

@interface NSDictionary(subscripts)
- (id)objectForKeyedSubscript:(id)key;
@end

@interface NSMutableDictionary(subscripts)
- (void)setObject:(id)obj forKeyedSubscript:(id <NSCopying>)key;
@end

@interface NSArray(subscripts)
- (id)objectAtIndexedSubscript:(NSUInteger)idx;
@end

@interface NSMutableArray(subscripts)
- (void)setObject:(id)obj atIndexedSubscript:(NSUInteger)idx;
@end

#endif