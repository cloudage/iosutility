//
//  UIColor+RGBA.h
//  dygz
//
//  Created by 张 琪 on 12-9-19.
//  Copyright (c) 2012年 多彩贵州印象. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (RGBA)
+(UIColor *)colorWithRGBA:(uint32_t)rgba;
+(UIColor *)colorWithIntRed:(NSUInteger)red green:(NSUInteger)green blue:(NSUInteger)blue alpha:(NSUInteger)alpha;
@end
