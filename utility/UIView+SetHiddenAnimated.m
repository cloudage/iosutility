//
//  UIView+SetHiddenAnimated.m
//  gywb
//
//  Created by zhang qi on 12-6-22.
//  Copyright (c) 2012年 Tiwer Studio. All rights reserved.
//

#import "UIView+SetHiddenAnimated.h"

@implementation UIView (SetHiddenAnimated)
-(void)setHidden:(BOOL)hidden animated:(BOOL)anim{
    if (!anim) {
        [self setHidden:hidden];
    }else {
        if (hidden == self.hidden) return;
        
        if (hidden) {
            self.alpha = 1;
            [UIView animateWithDuration:0.3
                             animations:^{
                                 self.alpha=0;
                             } 
                             completion:^(BOOL finished) {
                                 self.hidden = YES;
                             }];
        }else {
            self.alpha = 0;
            self.hidden = NO;
            [UIView animateWithDuration:0.3
                             animations:^{
                                 self.alpha=1;
                             }];
        }
    }
}
@end
