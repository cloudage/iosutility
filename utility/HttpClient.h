//
//  HttpClient.h
//  TestReflection
//
//  Created by zhang qi on 12-6-15.
//  Copyright (c) 2012年 Tiwer Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HttpParam : NSObject
@property (nonatomic,strong) NSString* name;
@property (nonatomic,strong) NSObject* value;
@property (nonatomic,assign) BOOL needUrlEncoding;
-(id)initWithValue:(NSObject*)value andName:(NSString*)name;
+(HttpParam*)paramWithValue:(NSObject*)value andName:(NSString*)name needUrlEncoding:(BOOL)urlEncoding;
+(HttpParam*)paramWithValue:(NSObject*)value andName:(NSString*)name;
@end

#define HTTP_FILE_TYPE_JPEG @"image/jpeg"
#define HTTP_FILE_TYPE_PNG @"image/png"

@interface HttpFileParam : HttpParam
@property (nonatomic,strong) NSData* fileData;
@property (nonatomic,strong) NSString* fileType;
-(id)initWithValue:(NSString*)filename
           andName:(NSString*)name
       andFileData:(NSData*)fileData
       andFileType:(NSString*)type;
@end

@interface HttpClient : NSObject{
@protected
    void(^onComplete)(NSData*);
    void(^onFailed)(NSError*);
}
+(HttpClient*)client;
+(HttpClient*)restClient;

@property (nonatomic,strong) NSArray* httpParameters;
@property (nonatomic,assign) Boolean isRest;
@property (nonatomic,strong) NSDictionary * httpHeaders;

-(void)addHttpParameters:(NSDictionary *)objects;
-(void)addHttpParameter:(HttpParam*)parameter;
-(void)removeHttpParameter:(HttpParam*)parameter;

-(void)setHttpHeader:(NSString*)value withName:(NSString*)name;

-(HttpParam*)parameterWithName:(NSString*)name;
-(void)addHttpParameterWithValue:(NSObject*)value andName:(NSString*)name;
-(void)addHttpUploadFileWithFileName:(NSString*)filename
                        andFieldName:(NSString*)name
                         andFileData:(NSData*)fileData
                         andFileType:(NSString*)fileType;

-(void)setComplicationBlock:(void(^)(NSData* data))blk;
-(void)setFailedBlock:(void(^)(NSError* error))blk;

-(void)startHttpGetFromURL:(NSURL*)url;
-(void)startHttpPostToURL:(NSURL*)url;
-(void)startHttpGetFromURLString:(NSString*)url;
-(void)startHttpPostToURLString:(NSString*)url;
-(void)startHttpUploadToURL:(NSURL*)url;
-(void)startHttpUploadToURLString:(NSString*)url;
@end

@interface NSData(StringWithEncoding)
@property (nonatomic,readonly) NSString* stringUsingUTF8Encoding;
-(NSString*)stringUsingEncoding:(NSStringEncoding)enc;
@end