//
//  NSString+Check.m
//  yd365
//
//  Created by 张琪 on 13-4-26.
//  Copyright (c) 2013年 多彩贵州印象. All rights reserved.
//

#import "NSString+Check.h"

@implementation NSString (Check)
-(BOOL)isPhoneNumber{
    for (int i=0; i<self.length; i++) {
        unichar c = [self characterAtIndex:i];
        if (c=='+' || c=='-' || c==' ') continue;
        if (c>='0' && c<='9') continue;
        return NO;
    }
    return YES;
}

-(BOOL)isAllNumber{
    for (int i=0; i<self.length; i++) {
        unichar c = [self characterAtIndex:i];
        if (c>='0' && c<='9') continue;
        return NO;
    }
    return YES;
}
@end
