//
//  LocalCache.h
//  gywb
//
//  Created by zhang qi on 12-6-18.
//  Copyright (c) 2012年 Tiwer Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData(LocalCache)
+(NSData*) dataCachedWithName:(NSString*)cacheName;
-(void) cacheWithName:(NSString*)cacheName;
+(NSString*) pathOfDataCachedWithName:(NSString*)cacheName;
+(void)cleanCache;
+(void)computeCacheFolderSize:(void(^)(NSUInteger size))callback;
@end
