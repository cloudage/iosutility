//
//  UIView+SetHiddenAnimated.h
//  gywb
//
//  Created by zhang qi on 12-6-22.
//  Copyright (c) 2012年 Tiwer Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (SetHiddenAnimated)
-(void)setHidden:(BOOL)hidden animated:(BOOL)anim;
@end
