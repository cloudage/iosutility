//
//  NSString+removeHtmlMarks.m
//  gywb
//
//  Created by zhang qi on 12-6-23.
//  Copyright (c) 2012年 Tiwer Studio. All rights reserved.
//

#import "NSString+removeHtmlMarks.h"

@implementation NSString (removeHtmlMarks)
-(NSString *)stringByRemovingHtmlMarks{
    NSString* html = self;
    NSScanner *theScanner;
    NSString *text = nil;        
    theScanner = [NSScanner scannerWithString:html];
    
    while ([theScanner isAtEnd] == NO) {
        [theScanner scanUpToString:@"<" intoString:NULL] ;
        [theScanner scanUpToString:@">" intoString:&text] ;
        html = [html stringByReplacingOccurrencesOfString:
                [ NSString stringWithFormat:@"%@>", text]
                                               withString:@" "];
    }
    
    return html;
    
}
@end
