//
//  UIView+Shadow.m
//  yd365
//
//  Created by 张琪 on 13-4-10.
//  Copyright (c) 2013年 多彩贵州印象. All rights reserved.
//

#import "UIView+Shadow.h"
#import "UIDevice+Hardware.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIView (Shadow)
-(void)applyShadowWithColor:(UIColor *)color
                 andOpacity:(CGFloat)opacity
                  andRadius:(CGFloat)radius
                  andOffset:(CGSize)offset{
    if ([UIDevice currentDevice].cpuCount < 2) {
        return;
    }
    self.layer.backgroundColor = self.backgroundColor.CGColor;
    self.layer.shadowColor = color.CGColor;
    self.layer.shadowOpacity = opacity;
    self.layer.shadowOffset = offset;
    self.layer.shadowRadius = radius;
}

-(BOOL)shouldRasterize{
    return self.layer.shouldRasterize;
}

-(void)setShouldRasterize:(BOOL)shouldRasterize{
    self.layer.shouldRasterize = shouldRasterize;
}

-(void)clearShadow{
    if ([UIDevice currentDevice].cpuCount < 2) {
        return;
    }
    self.layer.shadowColor = nil;
    self.layer.shadowOpacity = 0;
    self.layer.shadowOffset = CGSizeZero;
    self.layer.shadowRadius = 0;
}

@end
