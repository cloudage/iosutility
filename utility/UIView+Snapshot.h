//
//  UIView+Snapshot.h
//  yd365
//
//  Created by 张琪 on 13-4-25.
//  Copyright (c) 2013年 多彩贵州印象. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Snapshot)
-(UIImage*) imageOfSnapshot;
@end
