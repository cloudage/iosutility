//
//  SolidColorProgressView.m
//  yd365
//
//  Created by 张琪 on 13-4-27.
//  Copyright (c) 2013年 多彩贵州印象. All rights reserved.
//

#import "SolidColorProgressView.h"
#import <QuartzCore/QuartzCore.h>

@implementation SolidColorProgressView{
    UIView * _progressView;
    CGFloat _progress;
}

-(UIColor *)color{
    return _progressView.backgroundColor;
}

-(CGFloat)progress{
    return _progress;
}

-(void)setColor:(UIColor *)color{
    _progressView.backgroundColor = color;
    self.layer.borderColor = color.CGColor;
}

-(void)setProgress:(CGFloat)progress{
    _progress = progress;
    void(^adjustFrame)() = ^{
        CGRect f = _progressView.frame;
        f.size.width = self.bounds.size.width*progress;
        _progressView.frame = f;
    };
    
    if (self.window!=nil) {
        [UIView animateWithDuration:_progress/2 animations:adjustFrame];
    }else{
        adjustFrame();
    }
}

-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    self.layer.cornerRadius = self.bounds.size.height/2;
}

- (id)init
{
    return [self initWithFrame:CGRectMake(0, 0, 100, 10)];
}

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        CGRect f = self.bounds;
        f.size.width = 0;
        _progressView = [[UIView alloc] initWithFrame:f];
        _progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        [self addSubview:_progressView];
        
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = YES;
        
        self.color = [UIColor whiteColor];
        self.progress = 0;
    }
    return self;
}

@end
