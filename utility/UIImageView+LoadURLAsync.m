//
//  UIImageView+LoadURLAsync.m
//  dygz
//
//  Created by 张 琪 on 12-9-21.
//  Copyright (c) 2012年 多彩贵州印象. All rights reserved.
//

#import "Utility.h"

@implementation UIImageView (LoadURLAsync)

-(void)loadURLAsync:(NSURL *)url withDefaultImage:(UIImage *)defaultImage{

    self.image = defaultImage;
    
    if (url==nil || url.absoluteString.length==0) {
        return;
    }

    NSString* key = url.absoluteString.md5;
    
    NSData* local = [NSData dataCachedWithName:key];
    if (local!=nil) {
        self.image = [UIImage imageWithData:local];
        return;
    }
    
    DLog(@"Loading image from %@",url);
    
    UIView * block = [[UIView alloc] initWithFrame:self.bounds];
    {
        block.backgroundColor = [UIColor whiteColor];
        block.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        [self addSubview:block];
    }
    
    UIActivityIndicatorView * indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:(UIActivityIndicatorViewStyleGray)];
    {
        indicator.autoresizingMask =
        UIViewAutoresizingFlexibleLeftMargin|
        UIViewAutoresizingFlexibleRightMargin|
        UIViewAutoresizingFlexibleTopMargin|
        UIViewAutoresizingFlexibleBottomMargin;
        
        CGRect f = indicator.frame;
        f.origin.x = self.bounds.size.width/2 - f.size.width/2;
        f.origin.y = self.bounds.size.height/2 - f.size.height/2;
        indicator.frame = f;
    
        indicator.hidesWhenStopped = YES;
        
        [block addSubview:indicator];
        
        [indicator startAnimating];
    }

    HttpClient * client = [HttpClient client];
    
    [client setFailedBlock:^(NSError *error) {
        DLog(@"%@",error.localizedDescription);
        [block removeFromSuperview];
    }];
    
    [client setComplicationBlock:^(NSData *data) {
        UIImage* img = [UIImage imageWithData:data];
        if (img!=nil) {
            [data cacheWithName:url.absoluteString.md5];
        }

        self.image = img;
        
        [indicator stopAnimating];
        
        if (img==nil) {
            self.image = defaultImage;
            [block removeFromSuperview];
        }else{
            [UIView animateWithDuration:0.5
                             animations:^{
                                 block.alpha = 0;
                             }
                             completion:^(BOOL finished) {
                                 [block removeFromSuperview];
                             }];
        }
    }];
    
    [client startHttpGetFromURL:url];
}

@end