//
//  NSDictionary+optValue.m
//  yd365
//
//  Created by 张琪 on 13-4-15.
//  Copyright (c) 2013年 多彩贵州印象. All rights reserved.
//

#import "NSDictionary+optValue.h"
#import "Utility.h"

@implementation NSDictionary (optValue)
-(int)optIntWithKey:(id)key{
    id val = self[key];
    if ([val isKindOfClass:[NSNumber class]]) {
        NSNumber * num = val;
        return num.intValue;
    }
    if ([val isKindOfClass:[NSString class]]) {
        return [val intValue];
    }
    return 0;
}
-(float)optFloatWithKey:(id)key{
    id val = self[key];
    if ([val isKindOfClass:[NSNumber class]]) {
        NSNumber * num = val;
        return num.floatValue;
    }
    if ([val isKindOfClass:[NSString class]]) {
        return [val floatValue];
    }
    return 0;
}
-(long)optLongWithKey:(id)key{
    id val = self[key];
    if ([val isKindOfClass:[NSNumber class]]) {
        NSNumber * num = val;
        return num.longValue;
    }
    if ([val isKindOfClass:[NSString class]]) {
        return [val doubleValue];
    }
    return 0;
}
-(double)optDoubleWithKey:(id)key{
    id val = self[key];
    if ([val isKindOfClass:[NSNumber class]]) {
        NSNumber * num = val;
        return num.doubleValue;
    }
    if ([val isKindOfClass:[NSString class]]) {
        return [val doubleValue];
    }
    return 0;
}
-(NSString *)optStringWithKey:(id)key{
    NSObject * val = self[key];
    return [val description];
}
-(NSArray *)optArrayWithKey:(id)key{
    id val = self[key];
    if ([val isKindOfClass:[NSArray class]]) {
        return val;
    }
    return nil;
}
-(NSDictionary *)optDictionaryWithKey:(id)key{
    id val = self[key];
    if ([val isKindOfClass:[NSDictionary class]]) {
        return val;
    }
    return nil;
}
@end
