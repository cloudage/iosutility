//
//  UIImage+ScaleToSize.m
//  gywb
//
//  Created by zhang qi on 12-6-23.
//  Copyright (c) 2012年 Tiwer Studio. All rights reserved.
//

#import "UIImage+ScaleToSize.h"

@implementation UIImage (ScaleToSize)
-(UIImage *)imageScaledToSize:(CGSize)size{
    UIGraphicsBeginImageContext(size); 
    [self drawInRect:CGRectMake(0, 0, size.width, size.height)]; 
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext(); 
    UIGraphicsEndImageContext(); 
    return scaledImage; 
}
@end
