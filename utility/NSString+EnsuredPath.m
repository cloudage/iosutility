//
//  NSString+EnsuredPath.m
//  dygz
//
//  Created by 张 琪 on 12-10-31.
//  Copyright (c) 2012年 多彩贵州印象. All rights reserved.
//

#import "NSString+EnsuredPath.h"
#import "Log.h"
@implementation NSString (EnsuredPath)
-(NSString *)stringByAppendingPathComponent:(NSString *)str ensurePath:(Boolean)ensure{
    if (!ensure) {
        return [self stringByAppendingPathComponent:str];
    }else{
        NSString* path = [self stringByAppendingPathComponent:str];
        NSFileManager * fm = [NSFileManager defaultManager];
        
        BOOL isDir = NO;
        if ([fm fileExistsAtPath:path isDirectory:&isDir]) {
            if (isDir) {
                return path;
            }else{
                DLog(@"path %@ exsists but not a directory",path);
                return path;
            }
        }
        
        NSError* err = nil;
        [fm createDirectoryAtPath:path
      withIntermediateDirectories:YES
                       attributes:nil
                            error:&err];
        if (err!=nil) {
            DLog(@"%@",err);
            return nil;
        }
        
        return path;
    }
}
@end
