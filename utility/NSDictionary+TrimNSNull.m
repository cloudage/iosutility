//
//  NSDictionary+TrimNSNull.m
//  dygz
//
//  Created by 张 琪 on 12-10-11.
//  Copyright (c) 2012年 多彩贵州印象. All rights reserved.
//

#import "NSDictionary+TrimNSNull.h"

@implementation NSDictionary (TrimNSNull)
-(NSDictionary *)dictionaryByRemovingNSNull{
    NSMutableDictionary* mutdict = self.mutableCopy;
    [mutdict removeNSNull];
    return mutdict;
}
@end

@implementation NSMutableDictionary (TrimNSNull)
-(void)removeNSNull{
    NSArray* keys = self.allKeys.copy;
    for (id key in keys) {
        id obj = [self objectForKey:key];
        if ([obj isKindOfClass:[NSNull class]]) {
            [self removeObjectForKey:key];
        }
        else if ([obj isKindOfClass:[NSDictionary class]]) {
            [self setObject:[obj dictionaryByRemovingNSNull] forKey:key];
        }
        else if ([obj isKindOfClass:[NSArray class]]) {
            [self setObject:[obj arrayByRemovingNSNull] forKey:key];
        }
        else if ([obj isKindOfClass:[NSNumber class]] && [obj doubleValue]==0) {
            [self removeObjectForKey:key];
        }
    }
}
@end

@implementation NSArray (TrimNSNull)
-(NSArray *)arrayByRemovingNSNull{
    NSMutableArray* mut = self.mutableCopy;
    [mut removeNSNull];
    return mut;
}
@end

@implementation NSMutableArray (TrimNSNull)
-(void)removeNSNull{
    NSArray* cpy = self.copy;
    for (id e in cpy) {
        if ([e isKindOfClass:[NSNull class]]) {
            [self removeObject:e];
        }
        else if ([e isKindOfClass:[NSNumber class]] && [e boolValue]==NO) {
            [self removeObject:e];
        }
    }
    
    for (NSUInteger i=0; i<self.count; i++) {
        id e = [self objectAtIndex:i];
        if ([e isKindOfClass:[NSDictionary class]]) {
            [self replaceObjectAtIndex:i withObject:[e dictionaryByRemovingNSNull]];
        }
        else if([e isKindOfClass:[NSArray class]]) {
            [self replaceObjectAtIndex:i withObject:[e arrayByRemovingNSNull]];
        }
    }
}
@end