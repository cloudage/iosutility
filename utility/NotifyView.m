//
//  NotifyView.m
//  dygz
//
//  Created by 张 琪 on 12-9-27.
//  Copyright (c) 2012年 多彩贵州印象. All rights reserved.
//

#import "NotifyView.h"
#import <QuartzCore/QuartzCore.h>
#import "Utility.h"

@interface NotifyView(){
    UIActivityIndicatorView* _indicator;
    UILabel* _textLabel;
    SolidColorProgressView * _progressView;
    
    UIControl* _blockView;
    BOOL _showProgress;
    
    NSTimeInterval _idle;
}

@end

@implementation NotifyView

-(void)setText:(NSString *)text{
    _textLabel.text = text;
    _idle = 0;
    [self stateChanged];    
}

-(NSString *)text{
    return _textLabel.text;
}

-(void)setProgress:(CGFloat)progress{
    _progressView.progress = progress;
    [self stateChanged];
}

-(CGFloat)progress{
    return _progressView.progress;
}

-(void)setShowProgress:(BOOL)showProgress{
    _showProgress = showProgress;
    [self stateChanged];
}

-(BOOL)showProgress{
    return _showProgress;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

-(void)awakeFromNib{
    [self setup];
}

-(void)setup{
    self.layer.backgroundColor = [UIColor colorWithWhite:0 alpha:0.9].CGColor;
    self.layer.cornerRadius = 10;
    self.layer.masksToBounds = YES;
    
    if (_textLabel==nil) {
        _textLabel = [[UILabel alloc] initWithFrame:CGRectInset(self.bounds, 10, 10)];
        _textLabel.font = [UIFont boldSystemFontOfSize:16];
        _textLabel.textAlignment = UITextAlignmentCenter;
        _textLabel.textColor = [UIColor whiteColor];
        _textLabel.backgroundColor = [UIColor clearColor];
        _textLabel.numberOfLines = 0;
        _textLabel.lineBreakMode = UILineBreakModeWordWrap;
        [self addSubview:_textLabel];
        
        _indicator = [[UIActivityIndicatorView alloc]
                      initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [_indicator startAnimating];
        [self addSubview:_indicator];
        
        _progressView = [SolidColorProgressView new];
        _progressView.hidden = YES;
        [self addSubview:_progressView];
        
        _blockView = [[UIControl alloc] init];
        _blockView.backgroundColor = [UIColor blackColor];
        _blockView.alpha = 0;
        
        [self layout];
    }
}

-(void)layoutSubviews{
    [super layoutSubviews];
    [self layout];
}

-(void)stateChanged{
//    [self layout];
    
    if (_showProgress){
        [_progressView setHidden:NO animated:YES];
        _indicator.hidden = YES;
    }else{
        _progressView.hidden = YES;
        if (_textLabel.text.length==0 || _idle>=3) [_indicator setHidden:NO animated:YES];
        else _indicator.hidden = YES;
    }
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self layout];
                     }];
}

-(void)layout{
    CGFloat padding = 10;
    CGFloat offset = padding;
    
    {
        CGRect f = _indicator.frame;
        f.origin.x = (self.bounds.size.width - f.size.width) /2;
        f.origin.y = offset;
        
        if (!_indicator.hidden) offset += f.size.height + padding;
        else f.origin.y = -f.size.height;

        _indicator.frame = f;
    }

    {
        CGRect f = _progressView.frame;
        f.origin.y = offset;
        f.origin.x = 10;
        f.size.width = self.bounds.size.width - f.origin.x*2;
        
        if (!_progressView.hidden) offset += f.size.height + padding;
        else f.origin.y = -f.size.height;

        _progressView.frame = f;
    }
    
    {
        CGRect f = self.bounds;
        f.size.height = _textLabel.font.pointSize;
        f.origin.x = 10;
        f.size.width = self.bounds.size.width - f.origin.x*2;
        f.origin.y = offset;
        _textLabel.frame = f;
        
        if (_textLabel.text.length>0) offset += f.size.height + padding;
    }
    
    CGRect f = self.frame;
    f.origin.y -= (offset-f.size.height)/2;
    f.size.height = offset;
    self.frame = f;
}

-(void)showInWindow:(UIWindow*)window withFrame:(CGRect)displayFrame{
    if (self.window!=window) {
        [self removeFromSuperview];
        [window addSubview:self];
        [_blockView removeFromSuperview];
        [window addSubview:_blockView];
    }
    
    _blockView.frame = displayFrame;
    _blockView.alpha = 0;

    self.alpha = 0;
    CGRect f = self.frame;
    f.size.width = 200;
    f.origin.x = displayFrame.origin.x + (displayFrame.size.width - f.size.width) /2;
    f.origin.y = displayFrame.origin.y + (displayFrame.size.height - f.size.height) /2;
    self.frame = f;
    
    [self layout];

    [UIView animateWithDuration:0.2
                     animations:^{
                         self.alpha = 1;
                         _blockView.alpha = 0.1;
                     }];
    _idle = 0;
    [self countIdle];
}

-(void)countIdle{
    [self stateChanged];
    
    if (self.isShowing) {
        double delayInSeconds = 1.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self countIdle];
            _idle+=1;
        });
    }
}

-(void)showInView:(UIView *)view{
    if (view==nil) return;
    
    [self showInWindow:view.window withFrame:[view convertRect:view.bounds toView:view.window]];
}

-(Boolean)isShowing{
    return self.superview!=nil;
}

-(void)show{
    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    [self showInWindow:window withFrame:window.bounds];
}

-(void)dismiss{
    if (self.superview==nil) return;
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.alpha = 0;
                         _blockView.alpha = 0;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                         [_blockView removeFromSuperview];
                     }];
}

-(void)dismissAfterDelay:(NSTimeInterval)delay{
    [self performSelector:@selector(dismiss) withObject:nil afterDelay:delay];
}

+(void)showNotify:(NSString *)text inView:(UIView *)view{
    NotifyView * notify = [NotifyView new];
    notify.text = text;
    [notify showInView:view];
    [notify dismissAfterDelay:1];
}

@end
