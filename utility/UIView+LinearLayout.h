//
//  UIView+LinearLayout.h
//  yd365
//
//  Created by 张琪 on 13-4-22.
//  Copyright (c) 2013年 多彩贵州印象. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (LinearLayout)
-(void)layoutLinearly;
-(void)layoutLinearlyWithPadding:(CGSize)padding;
-(void)layoutLinearlyWithPadding:(CGSize)padding andGap:(CGFloat)gap;
-(void)layoutLinearlyWithPadding:(CGSize)padding shouldFitHeight:(BOOL)fitHeight;
-(void)layoutLinearlyWithPadding:(CGSize)padding andGap:(CGFloat)gap shouldFitHeight:(BOOL)fitHeight;
@end
