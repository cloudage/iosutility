//
//  NSDictionary+optValue.h
//  yd365
//
//  Created by 张琪 on 13-4-15.
//  Copyright (c) 2013年 多彩贵州印象. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (optValue)
- (int) optIntWithKey:(id)key;
- (float) optFloatWithKey:(id)key;
- (long) optLongWithKey:(id)key;
- (double) optDoubleWithKey:(id)key;
- (NSString*) optStringWithKey:(id)key;
- (NSArray*) optArrayWithKey:(id)key;
- (NSDictionary*) optDictionaryWithKey:(id)key;
@end
