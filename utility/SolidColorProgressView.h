//
//  SolidColorProgressView.h
//  yd365
//
//  Created by 张琪 on 13-4-27.
//  Copyright (c) 2013年 多彩贵州印象. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SolidColorProgressView : UIView
@property (nonatomic,assign) CGFloat progress;
@property (nonatomic,strong) UIColor * color;
@end
