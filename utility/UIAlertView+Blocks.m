//
//  UIAlertView+Blocks.m
//  dygz
//
//  Created by 张 琪 on 12-10-30.
//  Copyright (c) 2012年 多彩贵州印象. All rights reserved.
//

#import "UIAlertView+Blocks.h"

@class UIAlertViewBlock;

UIAlertViewBlock* blk = nil;

@interface UIAlertViewBlock : NSObject
<UIAlertViewDelegate>{
    @public
    void (^block)(int);
}
@end
@implementation UIAlertViewBlock
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    block(buttonIndex);
}
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    blk = nil;
}
@end

@implementation UIAlertView (Blocks)
-(void)setButtonClickBlock:(void(^)(int))onButtonClick{
    blk = [[UIAlertViewBlock alloc] init];
    blk->block = onButtonClick;
    self.delegate = blk;
}
@end
