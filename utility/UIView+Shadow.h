//
//  UIView+Shadow.h
//  yd365
//
//  Created by 张琪 on 13-4-10.
//  Copyright (c) 2013年 多彩贵州印象. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Shadow)
-(void)applyShadowWithColor:(UIColor*)color
                 andOpacity:(CGFloat)opacity
                  andRadius:(CGFloat)radius
                  andOffset:(CGSize)offset;
-(void)clearShadow;
@property (nonatomic,assign) BOOL shouldRasterize;
@end
