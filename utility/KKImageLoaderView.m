//
//  BBImageLoaderView.m
//  show
//
//  Created by zhang qi on 12-4-19.
//  Copyright (c) 2012年 Tiwer Studio. All rights reserved.
//

#import "KKImageLoaderView.h"
#import "HttpClient.h"
#import "Log.h"
#import "NSData+LocalCache.h"

@interface KKImageLoaderView(){
    UIImageView* _imageView;
    UIActivityIndicatorView* _progress;
    NSURL* _loadingUrl;
}
@end

@implementation KKImageLoaderView
@synthesize delegate = _delegate;
@synthesize placeHolderImage = _placeHolderImage;
@synthesize loadingUrl = _loadingUrl;

-(UIImage *)image{
    return _imageView.image;
}

-(void)setImage:(UIImage *)image{
    _imageView.image = image;
    [self swithcToImage];
}

-(void)setPlaceHolderImage:(UIImage *)placeHolderImage{
    _placeHolderImage = placeHolderImage;
    if (_imageView.image==nil) {
        _imageView.image = placeHolderImage;
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [_delegate imageLoaderTouched:self];
    [super touchesEnded:touches withEvent:event];
}

-(UIViewContentMode)contentMode{
    return _imageView.contentMode;
}

-(void)setContentMode:(UIViewContentMode)contentMode{
    _imageView.contentMode = contentMode;
}

-(void) layoutSubviews{
    _imageView.frame=self.bounds;
    _progress.frame=CGRectMake(self.bounds.size.width/2-_progress.bounds.size.width/2,
                               self.bounds.size.height/2-_progress.bounds.size.height/2,
                               _progress.bounds.size.width,
                               _progress.bounds.size.height);
}

-(void) switchToProgress{
    _imageView.image = _placeHolderImage;
    _progress.alpha=1;
    [_progress startAnimating];
}

-(void) swithcToImageAnimated{
    [UIView beginAnimations:nil context:nil];
    _progress.alpha=0;
    [UIView commitAnimations];
    [_progress stopAnimating];
}

-(void) swithcToImage{
    _progress.alpha=0;
    [_progress stopAnimating];
}

//-(void) switchToProgressAnimated{
//    [UIView beginAnimations:nil context:nil];
//    _imageView.alpha=0;
//    _progress.alpha=1;
//    [UIView commitAnimations];
//    [_progress startAnimating];
//}
//
//-(void) swithcToImageAnimated{
//    [UIView beginAnimations:nil context:nil];
//    _imageView.alpha=1;
//    _progress.alpha=0;
//    [UIView commitAnimations];
//    [_progress stopAnimating];
//}

-(void) setup{
    if (_imageView!=nil) return;
    _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    _progress = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _progress.color = [UIColor grayColor];
    
    self.clipsToBounds=YES;
    
    [self addSubview:_imageView];
    [self addSubview:_progress];
    
    [self layoutSubviews];
    
    [self switchToProgress];
}

- (id)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setup];
    }
    return self;
}

+(void)removeCacheForURL:(NSURL *)url{
    NSString* path = [NSData pathOfDataCachedWithName:url.absoluteString];
    if (path!=nil) {
        NSError* err = nil;
        [[NSFileManager defaultManager] removeItemAtPath:path error:&err];
        if (err!=nil) {
            DLog(@"%@",err.localizedDescription);
        }else{
            DLog(@"Cache Removed:%@",url);
        }
    }
}

-(void)loadImageFromURL:(NSURL *)url{
    if ([_loadingUrl isEqual:url]) {
        DLog(@"equals!");
        return;
    }
    _loadingUrl = url;
    
    if (url==nil) {
        _imageView.image=nil;
        return;
    }
    
    NSData* cached = [NSData dataCachedWithName:url.absoluteString];
    if (cached!=nil) {
        UIImage* img = [UIImage imageWithData:cached];
        _imageView.image = img;
        [self swithcToImage];
        return;
    }
    
    [self switchToProgress];
    
    CFAbsoluteTime startTime = CFAbsoluteTimeGetCurrent();
    
    HttpClient* client = [HttpClient client];
    [client setComplicationBlock:^(NSData *data) {
        if (_loadingUrl==url) {
            [data cacheWithName:url.absoluteString];
            
            _imageView.image = [UIImage imageWithData:data];
            
            if (CFAbsoluteTimeGetCurrent()-startTime>0.5) {
                [self swithcToImageAnimated];
            }else {
                [self swithcToImage];
            }
        }
    }];
    
    [client setFailedBlock:^(NSError * err) {
        DLog(@"%@",err);
    }];
    
    [client startHttpGetFromURL:url];
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
