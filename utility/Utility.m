//
//  Utility.m
//  dygz
//
//  Created by 张 琪 on 12-9-10.
//  Copyright (c) 2012年 多彩贵州印象. All rights reserved.
//

#import "Utility.h"

@implementation NSDictionary (subscripts)
-(id)objectForKeyedSubscript:(id)key{
    return [self objectForKey:key];
}
@end

@implementation NSMutableDictionary (subscripts)
-(void)setObject:(id)obj forKeyedSubscript:(id<NSCopying>)key{
    [self setObject:obj forKey:key];
}
@end

@implementation NSArray (subscripts)
-(id)objectAtIndexedSubscript:(NSUInteger)idx{
    return [self objectAtIndex:idx];
}
@end

@implementation NSMutableArray (subscripts)
-(void)setObject:(id)obj atIndexedSubscript:(NSUInteger)idx{
    [self insertObject:obj atIndex:idx];
}
@end
