//
//  UIAlertView+Blocks.h
//  dygz
//
//  Created by 张 琪 on 12-10-30.
//  Copyright (c) 2012年 多彩贵州印象. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (Blocks)
-(void)setButtonClickBlock:(void(^)(int))onButtonClick;
@end
