//
//  NSString+Check.h
//  yd365
//
//  Created by 张琪 on 13-4-26.
//  Copyright (c) 2013年 多彩贵州印象. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Check)
@property (nonatomic,readonly) BOOL isPhoneNumber;
@property (nonatomic,readonly) BOOL isAllNumber;
@end
