//
//  UIImage+ColorTransform.h
//  yd365
//
//  Created by 张琪 on 13-4-9.
//  Copyright (c) 2013年 多彩贵州印象. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ColorTransform)
- (UIImage*) grayscaleImage;

- (UIImage*) imageTintedWithColor:(UIColor*)color;
- (UIImage*) imageTintedWithColor:(UIColor*)color fill:(CGFloat)fraction;

+ (UIImage*) imageWithSize:(CGSize)sz andColor:(UIColor*)color;

+(UIImage *)imageWithSize:(CGSize)sz
  andLinearGradientColors:(NSArray *)colors
               colorStops:(CGFloat *)stops
                     from:(CGPoint)startPoint
                       to:(CGPoint)endPoint;
@end
