//
//  UIView+BorderColor.m
//  yd365
//
//  Created by 张 琪 on 13-4-12.
//  Copyright (c) 2013年 多彩贵州印象. All rights reserved.
//

#import "UIView+BorderColor.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIView (BorderColor)

-(UIColor *)borderColor{
    return [UIColor colorWithCGColor:self.layer.borderColor];
}

-(CGFloat)borderWidth{
    return self.layer.borderWidth;
}

-(CGFloat)cornerRadius{
    return self.layer.cornerRadius;
}

-(void)setBorderColor:(UIColor *)borderColor{
    self.layer.borderColor = borderColor.CGColor;
}

-(void)setBorderWidth:(CGFloat)borderWidth{
    self.layer.borderWidth = borderWidth;
}

-(void)setCornerRadius:(CGFloat)cornerRadius{
    self.layer.cornerRadius = cornerRadius;
    self.layer.masksToBounds = cornerRadius!=0;
}

@end
