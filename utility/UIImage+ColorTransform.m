//
//  UIImage+ColorTransform.m
//  yd365
//
//  Created by 张琪 on 13-4-9.
//  Copyright (c) 2013年 多彩贵州印象. All rights reserved.
//

#import "UIImage+ColorTransform.h"

@implementation UIImage (ColorTransform)
typedef enum {
    ALPHA = 0,
    BLUE = 1,
    GREEN = 2,
    RED = 3
} PIXELS;

-(UIImage *)grayscaleImage{
    CGSize size = [self size];
    int width = size.width;
    int height = size.height;
	
    // the pixels will be painted to this array
    uint32_t *pixels = (uint32_t *) malloc(width * height * sizeof(uint32_t));
	
    // clear the pixels so any transparency is preserved
    memset(pixels, 0, width * height * sizeof(uint32_t));
	
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	
    // create a context with RGBA pixels
    CGContextRef context = CGBitmapContextCreate(pixels, width, height, 8, width * sizeof(uint32_t), colorSpace,
                                                 kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedLast);
	
    // paint the bitmap to our context which will fill in the pixels array
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), [self CGImage]);
	
    for(int y = 0; y < height; y++) {
        for(int x = 0; x < width; x++) {
            uint8_t *rgbaPixel = (uint8_t *) &pixels[y * width + x];
			
            // convert to grayscale using recommended method: http://en.wikipedia.org/wiki/Grayscale#Converting_color_to_grayscale
            uint32_t gray = 0.3 * rgbaPixel[RED] + 0.59 * rgbaPixel[GREEN] + 0.11 * rgbaPixel[BLUE];
			
            // set the pixels to gray
            rgbaPixel[RED] = gray;
            rgbaPixel[GREEN] = gray;
            rgbaPixel[BLUE] = gray;
        }
    }
	
    // create a new CGImageRef from our context with the modified pixels
    CGImageRef image = CGBitmapContextCreateImage(context);
	
    // we're done with the context, color space, and pixels
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    free(pixels);
	
    // make a new UIImage to return
    UIImage *resultUIImage = [UIImage imageWithCGImage:image];
	
    // we're done with image now too
    CGImageRelease(image);
	
    return resultUIImage;
}

-(UIImage *)imageTintedWithColor:(UIColor *)color{
    return [self imageTintedWithColor:color fill:0];
}

-(UIImage *)imageTintedWithColor:(UIColor *)color fill:(CGFloat)fraction{
    if (color) {
        // Construct new image the same size as this one.
        UIImage *image;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 40000
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 4.0) {
            UIGraphicsBeginImageContextWithOptions([self size], NO, 0.0); // 0.0 for scale means "scale for device's main screen".
        }
#else
        if ([[[UIDevice currentDevice] systemVersion] floatValue] < 4.0) {
            UIGraphicsBeginImageContext([self size]);
        }
#endif
        CGRect rect = CGRectZero;
        rect.size = [self size];
        // Composite tint color at its own opacity.
        [color set];
        UIRectFill(rect);
        // Mask tint color-swatch to this image's opaque mask.
        // We want behaviour like NSCompositeDestinationIn on Mac OS X.
        [self drawInRect:rect blendMode:kCGBlendModeDestinationIn alpha:1.0];
        // Finally, composite this image over the tinted mask at desired opacity.
        if (fraction > 0.0) {
            // We want behaviour like NSCompositeSourceOver on Mac OS X.
            [self drawInRect:rect blendMode:kCGBlendModeSourceAtop alpha:fraction];
        }
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image;
    }
    return self;
}

+(UIImage *)imageWithSize:(CGSize)sz andColor:(UIColor *)color{
    @autoreleasepool{
        CGRect f = CGRectZero;
        f.size = sz;
        UIGraphicsBeginImageContext(sz);
        
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(ctx, color.CGColor);
        CGContextFillRect(ctx, f);
        
        UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        return img;        
    }
}

+(UIImage *)imageWithSize:(CGSize)sz
  andLinearGradientColors:(NSArray *)colors
               colorStops:(CGFloat *)stops
                     from:(CGPoint)startPoint
                       to:(CGPoint)endPoint{
    @autoreleasepool {
        if (colors.count==0) return nil;
        
        CGRect f = CGRectZero;
        f.size = sz;
        UIGraphicsBeginImageContext(sz);
        
        BOOL shouldReleaseStops = NO;
        if (stops==0) {
            stops = malloc(sizeof(CGFloat)*colors.count);
            CGFloat step = 1.0/(CGFloat)(colors.count-1);
            for (int i=0;i<colors.count;i++) {
                stops[i]=step*i;
            }
            shouldReleaseStops = YES;
        }
        
        CGContextRef ctx = UIGraphicsGetCurrentContext();

        CFMutableArrayRef cfcolors = CFArrayCreateMutable(CFAllocatorGetDefault(), colors.count, nil);
        for (UIColor * color in colors) {
            CFArrayAppendValue(cfcolors, color.CGColor);
        }
        
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        
        CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, cfcolors, stops);
        
        CGColorSpaceRelease(colorSpace);
        CFRelease(cfcolors);
        
        CGContextDrawLinearGradient(ctx, gradient, startPoint, endPoint, 0);
        
        CGGradientRelease(gradient);
        
        UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        if (shouldReleaseStops) {
            free(stops);
        }
        
        return img;
    }
}

@end
