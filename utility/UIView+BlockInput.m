//
//  UIView+BlockInput.m
//  yd365
//
//  Created by 张琪 on 13-4-10.
//  Copyright (c) 2013年 多彩贵州印象. All rights reserved.
//

#import "UIView+BlockInput.h"
#define BLOCK_TAG 6734523
@implementation UIView (BlockInput)
-(UIControl *)blockUserInput{
    UIControl * ctl = [[UIControl alloc] initWithFrame:self.bounds];
    ctl.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    ctl.tag = BLOCK_TAG;
    [self addSubview:ctl];
    return ctl;
}

-(void)unblockUserInput{
    for (UIView * blk in self.subviews) {
        if (blk.tag==BLOCK_TAG) {
            [blk removeFromSuperview];
        }
    }
}
@end
