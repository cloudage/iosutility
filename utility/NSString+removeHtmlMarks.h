//
//  NSString+removeHtmlMarks.h
//  gywb
//
//  Created by zhang qi on 12-6-23.
//  Copyright (c) 2012年 Tiwer Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (removeHtmlMarks)
@property (nonatomic,readonly) NSString* stringByRemovingHtmlMarks;
@end
