//
//  UIView+findFirstResponder.h
//  gywb
//
//  Created by zhang qi on 12-6-25.
//  Copyright (c) 2012年 Tiwer Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (findFirstResponder)
@property (nonatomic,readonly) UIView * rootView;
- (UIView*)findAndResignFirstResponder;
- (void)removeAllSubviews;
@end
