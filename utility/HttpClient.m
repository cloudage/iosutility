//
//  HttpClient.m
//  TestReflection
//
//  Created by zhang qi on 12-6-15.
//  Copyright (c) 2012年 Tiwer Studio. All rights reserved.
//

#import "HttpClient.h"
#import "NSString+MD5Addition.h"
#import "Log.h"

@implementation NSData(StringWithEncoding)
-(NSString *)stringUsingEncoding:(NSStringEncoding)enc{
    return [[NSString alloc] initWithData:self encoding:enc];
}
-(NSString *)stringUsingUTF8Encoding{
    return [[NSString alloc] initWithData:self encoding:NSUTF8StringEncoding];
}
@end






@implementation HttpParam
@synthesize name = _name;
@synthesize value = _value;
@synthesize needUrlEncoding = _needUrlEncoding;
- (id)initWithValue:(NSObject *)value andName:(NSString *)name
{
    self = [super init];
    if (self) {
        if (value==nil) {
            value = [NSNull null];
        }
        _value = value;
        _name = name;
        _needUrlEncoding = NO;
    }
    return self;
}
+(HttpParam *)paramWithValue:(NSObject *)value
                     andName:(NSString *)name
             needUrlEncoding:(BOOL)urlEncoding{
    HttpParam* p = [[HttpParam alloc] initWithValue:value andName:name];
    p.needUrlEncoding = urlEncoding;
    return p;
}
+(HttpParam *)paramWithValue:(NSObject *)value andName:(NSString *)name{
    return [HttpParam paramWithValue:value andName:name needUrlEncoding:YES];
}
-(NSString *)description{
    return [NSString stringWithFormat:@"%@ = %@",_name,_value];
}
@end





@implementation HttpFileParam
@synthesize fileData = _fileData;
@synthesize fileType = _fileType;
- (id)initWithValue:(NSString *)filename
            andName:(NSString *)name
        andFileData:(NSData *)fileData
        andFileType:(NSString *)type
{
    self = [super initWithValue:filename andName:name];
    _fileData = fileData;
    _fileType = type;
    return self;
}
-(NSString *)description{
    return [[super description] stringByAppendingFormat:@"(%@,%lu bytes)",_fileType,(unsigned long)_fileData.length];
}
@end






@interface HttpClient()
<NSURLConnectionDataDelegate>
{
    NSMutableArray* _params;

    NSMutableData* _data;
    
    NSArray* _cookies;
    
    BOOL _started;
}

@end

@implementation HttpClient
@synthesize httpParameters = _params;

+(HttpClient *)client{
    return [[HttpClient alloc] init];
}

+(HttpClient *)restClient{
    HttpClient* client = [self client];
    client.isRest = YES;
    return client;
}

-(void)setHttpHeader:(NSString *)value withName:(NSString *)name{
    NSMutableDictionary * mdic = _httpHeaders==nil ? [NSMutableDictionary new] : _httpHeaders.mutableCopy;

    if (value==nil) {
        [mdic removeObjectForKey:name];
    }else{
        mdic[name] = value;
    }

    _httpHeaders = mdic;
}

-(void)setComplicationBlock:(void (^)(NSData *))blk{
    onComplete = blk;
}

-(void)setFailedBlock:(void (^)(NSError *))blk{
    onFailed = blk;
}

- (id)init
{
    self = [super init];
    if (self) {
        _params = [NSMutableArray array];
        _started = NO;
    }
    return self;
}

-(void)dealloc{
    if (!_started) {
        DLog(@"a HttpClient released without started");
    }
}

-(void)addHttpParameters:(NSDictionary *)objects{
    for (id key in objects) {
        id val = objects[key];
        [self addHttpParameterWithValue:val andName:key];
    }
}

-(void)addHttpParameter:(HttpParam *)parameter{
    [_params addObject:parameter];
}

-(void)removeHttpParameter:(HttpParam *)parameter{
    [_params removeObject:parameter];
}

-(HttpParam *)parameterWithName:(NSString *)name{
    for (HttpParam* p in _params) {
        if (p.name==name || [p.name isEqual:name]) {
            return p;
        }
    }
    
    return nil;
}

-(void)addHttpParameterWithValue:(NSObject*)value andName:(NSString*)name{
    if (value==nil||name==nil) return;
    
    HttpParam* param = [HttpParam paramWithValue:value andName:name needUrlEncoding:YES];
    [self addHttpParameter:param];
}

-(void)addHttpUploadFileWithFileName:(NSString*)filename
                        andFieldName:(NSString*)name
                         andFileData:(NSData*)fileData
                         andFileType:(NSString*)fileType{
    filename = [filename stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    name = [name stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    HttpFileParam* fileParam = [[HttpFileParam alloc] initWithValue:filename
                                                            andName:name
                                                        andFileData:fileData
                                                        andFileType:fileType];
    [self addHttpParameter:fileParam];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSHTTPURLResponse *)response{
    if (response.expectedContentLength>0) {
        DLog(@"receive %lld bytes",response.expectedContentLength);
    }
    
    _data = [NSMutableData data];
    _cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:response.allHeaderFields forURL:response.URL];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    DLog(@"receive %lu bytes",(unsigned long)data.length);
    [_data appendData:data];
}

-(void)connection:(NSURLConnection *)connection 
  didSendBodyData:(NSInteger)bytesWritten
totalBytesWritten:(NSInteger)totalBytesWritten
totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite{
    DLog(@"post data %ld / %ld bytes",(long)totalBytesWritten,(long)totalBytesExpectedToWrite);
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    DLog(@"failed %@",error);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (onFailed!=nil) {
            onFailed(error);
        }
    });
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    DLog(@"finished");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (onComplete!=nil) {
            onComplete([NSData dataWithData:_data]);
        }
    });
}

-(void)startHttpGetFromURLString:(NSString*)urlStr{
    NSURL* url = [NSURL URLWithString:urlStr];
    [self startHttpGetFromURL:url];
}

-(void)startHttpPostToURLString:(NSString*)urlStr{
    NSURL* url = [NSURL URLWithString:urlStr];
    [self startHttpPostToURL:url];    
}

-(void)startHttpUploadToURLString:(NSString *)urlStr{
    NSURL* url = [NSURL URLWithString:urlStr];
    [self startHttpUploadToURL:url];
}

-(void)startHttpGetFromURL:(NSURL *)url{
    _started = YES;
    if (url==nil) {
        if (onFailed!=nil) {
            NSDictionary* uinf = [NSDictionary dictionaryWithObjectsAndKeys:
                                  @"url not set", @"why",
                                  nil];
            NSError* err = [NSError errorWithDomain:@"App" code:100 userInfo:uinf];
            onFailed(err);
        }
        return;
    }
    
    if (_params!=nil && _params.count>0) {
        NSMutableString* queryStr = [NSMutableString string];
        for (HttpParam* p in _params) {
            NSString* segment;
            if (_isRest) {
                segment = [NSString stringWithFormat:@"/%@/%@",p.name,p.value];
            }else{
                segment = [NSString stringWithFormat:@"&%@=%@",p.name,p.value];
            }
            
            if (p.needUrlEncoding) {
                segment = [segment stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            }
            [queryStr appendString:segment];
        }

        if (!_isRest) {
            [queryStr replaceCharactersInRange:NSMakeRange(0, 1) withString:@"?"];
        }
        
        NSString* urlstr = [url.absoluteString stringByAppendingString:queryStr];
        url =[NSURL URLWithString:urlstr];
    }
    
    if (url==nil) {
        if (onFailed!=nil) {
            NSDictionary* uinf = [NSDictionary dictionaryWithObjectsAndKeys:
                                  @"failed convert params into query string", @"why",
                                  _params,@"what",
                                  nil];
            NSError* err = [NSError errorWithDomain:@"App" code:101 userInfo:uinf];
            onFailed(err);
        }
        return;
    }
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    request.timeoutInterval = 60;
    
    if (_httpHeaders.count>0) {
        for (id key in _httpHeaders) {
            [request addValue:_httpHeaders[key] forHTTPHeaderField:key];
        }
    }
    
    if (_cookies!=nil) {
        for (NSHTTPCookie* cookie in _cookies) {
            [request addValue:[cookie value] forHTTPHeaderField:@"Cookies"];
        }
    }
    
    [NSURLConnection connectionWithRequest:request delegate:self];
    DLog(@"get from %@",request.URL.absoluteString);
}

-(void)startHttpPostToURL:(NSURL *)url{
    for (HttpParam* p in _params) {
        if ([p isKindOfClass:[HttpFileParam class]]) {
            [self startHttpUploadToURL:url];
            return;
        }
    }
    
    _started = YES;
    if (url==nil) {
        if (onFailed!=nil) {
            NSDictionary* uinf = [NSDictionary dictionaryWithObjectsAndKeys:
                                  @"url not set", @"why",
                                  nil];
            NSError* err = [NSError errorWithDomain:@"App" code:100 userInfo:uinf];
            onFailed(err);
        }
        return;
    }
    
    NSData* postBody = nil;
    
    if (_params!=nil && _params.count>0) {
        NSMutableString* queryStr = [NSMutableString string];
        for (HttpParam* p in _params) {
            NSString* segment = [NSString stringWithFormat:@"&%@=%@",p.name,p.value];
            if (p.needUrlEncoding) {
                segment = [segment stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            }
            [queryStr appendString:segment];
        }
        postBody = [queryStr dataUsingEncoding:NSUTF8StringEncoding];
    }
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPBody = postBody;
    request.timeoutInterval = 60;
    
    if (_httpHeaders.count>0) {
        for (id key in _httpHeaders) {
            [request addValue:_httpHeaders[key] forHTTPHeaderField:key];
        }
    }
    
    if (_cookies!=nil) {
        for (NSHTTPCookie* cookie in _cookies) {
            [request addValue:[cookie value] forHTTPHeaderField:@"Cookies"];
        }
    }
    
    request.HTTPMethod = @"POST";
    
    [NSURLConnection connectionWithRequest:request delegate:self];
    DLog(@"post to %@ \nwith params:\n%@",request.URL.absoluteString,_params);
}

-(void)startHttpUploadToURL:(NSURL *)url{
    _started = YES;
    
    if (url==nil) {
        if (onFailed!=nil) {
            NSDictionary* uinf = [NSDictionary dictionaryWithObjectsAndKeys:
                                  @"url not set", @"why",
                                  nil];
            NSError* err = [NSError errorWithDomain:@"App" code:100 userInfo:uinf];
            onFailed(err);
        }
        return;
    }

    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    
    if (_httpHeaders.count>0) {
        for (id key in _httpHeaders) {
            [request addValue:_httpHeaders[key] forHTTPHeaderField:key];
        }
    }
    
    if (_cookies!=nil) {
        for (NSHTTPCookie* cookie in _cookies) {
            [request addValue:[cookie value] forHTTPHeaderField:@"Cookies"];
        }
    }
    
    if (_params!=nil) {
        NSString* boundaryStr = [[[NSDate date] description] stringFromMD5];
        boundaryStr = [NSString stringWithFormat:@"---------------------------%@",boundaryStr];

        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundaryStr];
        [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
        
        boundaryStr = [NSString stringWithFormat:@"\r\n--%@\r\n",boundaryStr];
        NSData* boundaryData = [boundaryStr dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableData* httpBody = [NSMutableData data];
        
        for (id p in _params) {
            [httpBody appendData:boundaryData];
            if ([p isKindOfClass:[HttpFileParam class]]) {
                HttpFileParam* fileParam = p;                
                
                NSString* disp = [NSString stringWithFormat:@"Content-Disposition: form-data; name='%@'; filename='%@'\r\n",
                                  fileParam.name,fileParam.value];
                [httpBody appendData:[disp dataUsingEncoding:NSUTF8StringEncoding]];
                
                NSString* dt = [NSString stringWithFormat:@"Content-Type: %@\r\n\r\n",fileParam.fileType];
                [httpBody appendData:[dt dataUsingEncoding:NSUTF8StringEncoding]];
                
                [httpBody appendData:fileParam.fileData];
            }else {
                HttpParam* param = p;
                
                NSString* disp = [NSString stringWithFormat:@"Content-Disposition: form-data; name='%@'\r\n",param.name];                
                [httpBody appendData:[disp dataUsingEncoding:NSUTF8StringEncoding]];
                
                [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

                [httpBody appendData:[param.value.description dataUsingEncoding:NSUTF8StringEncoding]];
            }            
            [httpBody appendData:boundaryData];
        }
        
        request.HTTPBody = httpBody;
    }
    request.HTTPMethod = @"POST";
    [NSURLConnection connectionWithRequest:request delegate:self];
    DLog(@"post to %@ \nwith params:\n%@",request.URL.absoluteString,_params);
}
@end
