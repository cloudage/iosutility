//
//  LocalCache.m
//  gywb
//
//  Created by zhang qi on 12-6-18.
//  Copyright (c) 2012年 Tiwer Studio. All rights reserved.
//

#import "NSData+LocalCache.h"
#import "NSString+MD5Addition.h"
#import "Log.h"

@implementation NSData(LocalCache)

NSURL* cacheStorageURLWithCacheName(NSString* cacheName)
{
    if (cacheName==nil) return nil;
    
    NSURL* url = [[[NSFileManager defaultManager]
                   URLsForDirectory:NSCachesDirectory
                   inDomains:NSUserDomainMask] lastObject];
    
    url = [url URLByAppendingPathComponent:cacheName.stringFromMD5
                               isDirectory:NO];    
    return url;
}

+(NSData *)dataCachedWithName:(NSString *)cacheName{
    NSURL* url = cacheStorageURLWithCacheName(cacheName);
    if (url==nil) return nil;
    return [NSData dataWithContentsOfMappedFile:url.path];
}

+(NSString *)pathOfDataCachedWithName:(NSString *)cacheName{
    NSURL* url = cacheStorageURLWithCacheName(cacheName);
    if (url==nil) return nil;
    return url.path;
}

-(void)cacheWithName:(NSString *)cacheName{
    if (cacheName==nil) return;
    NSURL* url = cacheStorageURLWithCacheName(cacheName);
    [self writeToURL:url atomically:YES];
}

+(void)cleanCache{
    NSFileManager* fm = [NSFileManager defaultManager];
    NSURL* url = [[[NSFileManager defaultManager]
                   URLsForDirectory:NSCachesDirectory
                   inDomains:NSUserDomainMask] lastObject];
    NSString* path = url.path;
    NSDirectoryEnumerator* en = [fm enumeratorAtPath:path];
    NSError* err = nil;
    BOOL res;
    
    NSString* file;
    while (file = [en nextObject]) {
        res = [fm removeItemAtPath:[path stringByAppendingPathComponent:file] error:&err];
        if (err!=nil) {
            DLog(@"error: %@", err);
        }
    }
}

+(void)computeCacheFolderSize:(void(^)(NSUInteger size))callback{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSFileManager* fm = [NSFileManager defaultManager];
        NSURL* url = [[[NSFileManager defaultManager]
                       URLsForDirectory:NSCachesDirectory
                       inDomains:NSUserDomainMask] lastObject];
        NSString* path = url.path;
        
        NSArray *filesArray = [fm subpathsOfDirectoryAtPath:path error:nil];
        NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
        NSString *fileName;
        NSUInteger fileSize = 0;
        
        while (fileName = [filesEnumerator nextObject]) {
            NSString* fullFileName = [path stringByAppendingPathComponent:fileName];
            
            NSError* err = nil;
            NSDictionary *fileDictionary = [fm attributesOfItemAtPath:fullFileName
                                                                error:&err];
            if (err!=nil) {
                DLog(@"error: %@",err);
            }
            fileSize += [fileDictionary fileSize];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            callback(fileSize);
        });
    });
}
@end
